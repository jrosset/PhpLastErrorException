<?php

use jrosset\LastErrorException\LastErrorException;

require_once __DIR__ . '/../vendor/autoload.php';

try {
    if (!file_put_contents('/test_file', 'Some data')) {
        throw LastErrorException::createFromLastError();
    }
}
catch (Throwable $e) {
    fwrite(
        STDERR,
        'EXCEPTION [' . get_class($e) . '] ' . $e->getMessage()
        . PHP_EOL . 'in ' . $e->getFile() . '(' . $e->getLine() . ')'
        . PHP_EOL . $e->getTraceAsString()
    );
}