<?php

namespace jrosset\LastErrorException;

use ErrorException;

/**
 * Exception based on {@see https://www.php.net/manual/function.error-get-last.php error_get_last()}
 */
class LastErrorException extends ErrorException {
    /**
     * Constructs the exception
     *
     * @param int $code The exception code
     */
    public function __construct (int $code = 0) {
        if (($lastError = error_get_last()) === null) {
            $lastError = [
                'type'    => 0,
                'message' => '',
                'file'    => __FILE__,
                'line'    => __LINE__,
            ];
        }
        parent::__construct($lastError['message'], $code, $lastError['type'], $lastError['file'], $lastError['line']);
    }

    /**
     * Create the exception if there is a last error
     *
     * @param bool $checkErrorReporting Check if last error type match {@see https://www.php.net/manual/function.error-reporting error_reporting()}
     *
     * @return static|null The exception or Null if no last error
     */
    public static function createFromLastError (bool $checkErrorReporting = true): ?self {
        if (($lastError = error_get_last()) === null) {
            return null;
        }
        if ($checkErrorReporting && !($lastError['type'] & error_reporting())) {
            return null;
        }
        return new static();
    }
}